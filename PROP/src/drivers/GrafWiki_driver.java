package drivers;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class GrafWiki_driver {

        static GrafWiki gw;

        private static void Inici() {
                System.out.println("1: public GrafWiki()");
                System.out.println("2: public int size()");
                System.out.println("3: public LiniaWiki ConsultarIessim(int i)");
                System.out.println("4: public void AfegirLinia(LiniaWiki l)");
                System.out.println("5: public boolean EsborrarLinia(LiniaWiki l)");
                System.out.println("6: public void EsborrarLinia_i(int i)");
                System.out.println("7: public void EsborrarArestesDeNode(String node, String tipus)");
                System.out.println("8: public Iterator<LiniaWiki> iterator()");
                System.out.println("9: public Iterator<LiniaWiki> iterator(int i) throws DadesIncorrectesException");
                System.out.println("-1: Sortir");
        }
        
        public static void main(String[] args) {
        
                Inici();
                int i; 
                Scanner s = new Scanner(System.in);
                
                i = s.nextInt();
                while (i != -1) {
                        switch(i) {
                                case 1:
                                        gw = new GrafWiki();
                                        System.out.println("Graf creat!");
                                        break;
                                case 2:
                                        System.out.println(gw.size());
                                        break;                                
                                case 3:
                                        System.out.println("Introduir linia a consultar");
                                        System.out.println(gw.ConsultarIessim(s.nextInt()).ObtenirLinia());
                                        break;
                                case 4:
                                        System.out.println("Introduir linia a afegir");
                                        LiniaWiki l1 = new LiniaWiki(s.next(),s.next(),s.next(),s.next(),s.next());
                                        gw.AfegirLinia(l1);
                                        System.out.println("Linia afegida");
                                        break;
                                case 5:
                                        System.out.println("Introduir linia a esborrar");
                                        LiniaWiki l2 = new LiniaWiki(s.next(),s.next(),s.next(),s.next(),s.next());
                                        gw.EsborrarLinia(l2);
                                        System.out.println("Linia esborrada");
                                        break;
                                case 6:
                                        System.out.println("Introduir numero de linia a esborrar");
                                        gw.EsborrarLinia_i(s.nextInt());
                                        System.out.println("Linia esborrada");
                                        break;
                                case 7:
                                        System.out.println("Introduir node a borrar");
                                        gw.EsborrarArestesDeNode(s.next(),s.next());
                                        System.out.println("Node esborrat");
                                        break;
                                case 8:
                                        Iterator<LiniaWiki> it1 = gw.iterator();
                                        while (it1.hasNext()) {
                                                System.out.println(it1.next().ObtenirLinia());
                                        }
                                        break;
                                case 9:
                                        Iterator<LiniaWiki> it2;
                                        try {
                                                it2 = gw.iterator(4);
                                                while (it2.hasNext()) {
                                                System.out.println(it2.next().ObtenirLinia());
                                        }
                                        }
                                        catch (Exception e) {}
                                        break;
                        }
                        i = s.nextInt();
                }
                s.close();
        }
}
