package drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

public class Label_driver {
	static Graf<ElemWiki,ArestaPes,Double> graf_pes;
	static void menu_inici(){
		System.out.println("LABEL");
		System.out.println("Creadora");
		System.out.println("1: Label()");
		System.out.println("Modificadora");
		System.out.println("2: Cercar_Comunitats(Entrada En, Sortida So)");
		System.out.println("3: Imprimir resultats");
		System.out.println("-1: sortir");
	}
	public static void main(String[] args)throws FileNotFoundException {
		// TODO Auto-generated method stub
		menu_inici();
		Scanner s = new Scanner(System.in);
		int i = 0;
		graf_pes = new Graf<ElemWiki,ArestaPes,Double>(true);
		
		boolean c;
		String nom;
		ElemWiki e1;
		i = s.nextInt();
		
		while (i != -2) {
			System.out.println("Afegint Vertex:");
			c = s.nextBoolean();
			nom = s.next();
			if(c) e1 = new Pagina(nom);
			else e1 = new Categoria(nom);
			graf_pes.AfegirVertex(e1);
			i = s.nextInt();
		}
		
		
		i = s.nextInt();
		while (i != -2) {
			System.out.println("Afegint Arestes:");
			graf_pes.AfegirArestaNum(s.nextInt(), s.nextDouble(), s.nextInt());
			i = s.nextInt();
		}
		Entrada En = new Entrada(graf_pes);
		Sortida So = new Sortida();
		Label La;
		while (i != -1){
			switch (i){
				case 1:
					System.out.println("Constructora");
					break;
				case 2:
					System.out.println("Results");
					La = new Label(En,So);	
					break;
				case 3:
					List<Graf> Grafs = new ArrayList<Graf>();
					Grafs = So.Comunitats_resultat();
					for (int z = 0; z < Grafs.size(); ++z) {
						List <ElemWiki> vertexs = new ArrayList <ElemWiki>();
						vertexs = Grafs.get(z).ListaVertex();
						for (int j = 0; j < vertexs.size(); ++j) {
							System.out.println(vertexs.get(j).NomNode());
						}
						System.out.println("::::::::::::::::");
					}
					break;
			}
			i = s.nextInt();
		}
		s.close();
	}
}
