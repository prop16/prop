package drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import domini.ArestaPes;
import domini.ArestaWiki;
import domini.Categoria;
import domini.ElemWiki;
import domini.Graf;
import domini.Jaro_winkler;
import domini.Pagina;
import domini.Parell;


public class JaroWinkler_driver {
	static Graf<ElemWiki,ArestaPes,Double> graf_pes;
	static Graf<ElemWiki,ArestaWiki,String> graf_wiki;
	static Jaro_winkler jw;
	static void menu_inici(){
		System.out.println("Driver de Jaro Winkler");
		System.out.println("Creadora:");
		System.out.println("1: g (graf d'entrada) : Jaro_winkler(Graf<ElemWiki,ArestaWiki,String> g)");
		System.out.println("Modificadores:");
		System.out.println("2: p : void modificar_p(double pes)");
		System.out.println("3: llin : void modificar_llindar(double llin)");
		System.out.println("4: void valors_per_defecte()");
		System.out.println("5: void convert()");
		System.out.println("6: g : void modificar_graf_input(Graf<ElemWiki,ArestaWiki,String> g)");
		System.out.println("Consultores:");		
		System.out.println("7: double llindar_winkler()");
		System.out.println("8: double p_winkler()");
		System.out.println("9: Graf<ElemWiki,ArestaPes,Double> obtenir_graf()");
		System.out.println("10: Crear i modificar graf g input");
		System.out.println("11: imprimir graf output");
		System.out.println("-1: sortir");
	}
		
	public static void main(String[] args) throws FileNotFoundException {
		Scanner sc = new Scanner(System.in);
		menu_inici();
		int x = sc.nextInt();
		graf_wiki = new Graf<ElemWiki,ArestaWiki,String>(false);
		while(x != -1){
			switch (x) {
				case 1:
					jw = new Jaro_winkler(graf_wiki);
					break;
				case 2:
					double pes = sc.nextDouble();
					jw.modificar_p(pes);
					break;
					
				case 3:
					double llindar = sc.nextDouble();
					jw.modificar_llindar(llindar);
					break;
				case 4:
					jw.valors_per_defecte();
					break;
				case 5:
					jw.convert();
					break;
				case 6:
					jw.modificar_graf_input(graf_wiki);
					break;
				case 7:
					System.out.println("Llindar: " + jw.llindar_winkler());
					break;
				case 8:
					System.out.println("pes: " + jw.p_winkler());
					break;
				case 9:
					graf_pes = jw.obtenir_graf();
					break;
				case 10:
					System.out.println("Entra els vertex en el format vertex, pg(boolean que indica 1 pagina 0 categoria)");
					System.out.println("quan acabis escriu -1");
					String v = sc.next();
					
					while(!v.equals("-1")){
						boolean pg = sc.nextBoolean();
						ElemWiki Vertex;
						if(pg)Vertex = new Pagina(v);
						else Vertex = new Categoria(v);
						graf_wiki.AfegirVertex(Vertex);
						v = sc.next();
					}
					System.out.println("Entra les arestes en format elem_origen(v,pg), nom_link , elem_desti(v,pg)");
					System.out.println("Quan acabis escriu -1");
					v = sc.next();
					while(!v.equals("-1")){
						boolean pgo = sc.nextBoolean();
						String link = sc.next();
						String nom_desti = sc.next();
						boolean pgd = sc.nextBoolean();
						ElemWiki origen;
						ElemWiki desti;
						if(pgo) origen = new Pagina(v);
						else origen = new Categoria(v);
						if(pgd) desti = new Pagina(nom_desti);
						else desti = new Categoria(nom_desti);
						graf_wiki.AfegirAresta(origen, link, desti);
						v = sc.next();
					}
					break;
					
				case 11:
					int n = graf_pes.ordre();
					boolean[][] visitades = new boolean[n][n];
					for(int i = 0; i < n; ++i) Arrays.fill(visitades[i],false);
					for(int i = 0; i < graf_pes.ordre();++i){
						List<Parell<Integer,Double>> l = graf_pes.ArestesAdjacents(i);
						for(int j = 0; j < l.size(); ++j ){
							
							if(!visitades[i][l.get(j).primer]) {
								visitades[i][l.get(j).primer] = true;
								visitades[l.get(j).primer][i] = true;
								System.out.println("Origen " + graf_pes.vertex(i).NomNode() + " pes " + l.get(j).segon + " desti " + graf_pes.vertex(l.get(j).primer));
								
							}
						
							
						}
					}
					break;

			}
			x = sc.nextInt();
		}
		sc.close();
		
	}
}
