package drivers;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import domini.*;

public class Graf_Driver {
	static Graf<ElemWiki,ArestaPes,Double> graf_pes;
	static Graf<ElemWiki,Relacio,String> graf_wiki;
	static boolean b;//indica amb quina representacio de graf treballem

	static void menu_inici(){
		System.out.println("Creadores:");
		System.out.println("1: pes_aresta : Graf<V,A,E>(boolean pes_aresta)");
		System.out.println("2: n(numero d'elements de la llista) l (per simplificar g sera el graf que hi hagi al driver) : Graf<V,A,E>(Graf<V,A,E> g, List<Integer> l)");
		System.out.println("Modificadores:");
		System.out.println("3: pg(si pg el vertex es una pagina, sino una categoria), vertex : void AfegirVertex(ElemWiki vertex)");
		System.out.println("4: pg, vertexO, contingut, pg, vertexD : void AfegirAresta(ElemWiki vertexO, E contingut, ElemWiki vertexD)");
		System.out.println("5: vertexO, pes,vertexD : void AfegirAresta(int vertexO, Double pes, int vertexD)");
		System.out.println("6: pg, nome1, pg, nome2, s(creadora d'aresta per la seguent operecio) : ArestaPes(ElemWiki e1, ElemWiki e2, Double s)");
		System.out.println("7: pg, nome1, pg, nome2, s(creadora d'aresta per la seguent operecio) : Relacio(ElemWiki e1, ElemWiki e2)");
		System.out.println("8: (a = aresta creada en alguna de les dues opcions anteriors)void AfegirAresta(A a)");
		System.out.println("9: v : void BorrarVertex(ElemWiki v)");
		System.out.println("10: vertexO, vertexD : void BorrarAresta(V vertexO, V vertexD)");
		System.out.println("Consultores:");		
		System.out.println("11: int ordre()");
		System.out.println("12: boolean mode()");
		System.out.println("13: int mida()");
		System.out.println("14: v : int grau(ElemWiki v)");
		System.out.println("15: v : int grau(int v)");
		System.out.println("16: i : ElemWiki vertex(int i)");
		System.out.println("17: List<ElemWiki> ListaVertex()");
		System.out.println("18: v : List<Parell<Integer,E>> ArestesAdjacents(int v)");
		System.out.println("19: v : List<Parell<Integer,E>> ArestesAdjacents(V v)");
		System.out.println("20: v : List<ElemWiki> VertexAdjacents(V v)");
		System.out.println("21: v : List<Integer> VertexAdjacents(int v)");
		System.out.println("22: vO, vD : boolean ExisteixAresta(int vO,int vD)");
		System.out.println("23: pg, v0, pg, vD : boolean ExisteixAresta(ElemWiki vO, ElemWiki vD)");
		System.out.println("24: v1, v2 :Double PesAresta(int v1, int v2)");
		System.out.println("25: List<Integer> Arrels()");
		System.out.println("26: List<Parell<ElemWiki,List<String>>> ArestesAdjacentsDirigit(ElemWiki v)");
		System.out.println("-1: sortir");
	}
		
	public static void main(String[] args) throws FileNotFoundException {
		Scanner sc = new Scanner(System.in);
		
		String nom;
		ElemWiki e1;
		ElemWiki e2;
		boolean c;
		Aresta a = null;
		
		int x = sc.nextInt();
		while(x != -1){
			switch (x) {
				case 1:
					b = sc.nextBoolean();
					if(b)graf_pes = new Graf<ElemWiki,ArestaPes,Double>(b);
					else graf_wiki = new Graf<ElemWiki,Relacio,String>(b);
					break;
				case 2:
					int n = sc.nextInt();
					List<Integer> l = new ArrayList<Integer>();
					for(int i = 0; i < n; ++i){
						l.add(sc.nextInt());
					}
					if(b) {
						Graf<ElemWiki,ArestaPes,Double> graf_pes1 = new Graf<ElemWiki,ArestaPes,Double>(graf_pes,l);
						graf_pes = graf_pes1;
					}
					else {
						Graf<ElemWiki,Relacio,String> graf_wiki1 = new Graf<ElemWiki,Relacio,String>(graf_wiki, l);
						graf_wiki = graf_wiki1;
					}
					break;
				case 3:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					if(b)graf_pes.AfegirVertex(e1);
					else graf_wiki.AfegirVertex(e1);
					break;
				case 4:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					
					Double d=null;
					String s=null;
					if(b) d = sc.nextDouble();
					else s = sc.next();
					
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e2 = new Pagina(nom);
					else e2 = new Categoria(nom);
					
					if(b) graf_pes.AfegirAresta(e1,d, e2);
					else graf_wiki.AfegirAresta(e1, s, e2);
					break;
				case 5:
					if(b){						
						graf_pes.AfegirAresta(sc.nextInt(), sc.nextDouble(), sc.nextInt());
					}
					break;
				case 6:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e2 = new Pagina(nom);
					else e2 = new Categoria(nom);
					a = new ArestaPes(e1, e2, sc.nextDouble());
					break;
				case 7:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e2 = new Pagina(nom);
					else e2 = new Categoria(nom);
					String aux = sc.next();
					if (aux.compareTo("PP") == 0) 
					     a = new RelacioPP(e1, e2);
					else if (aux.compareTo("CP") == 0)
					     a = new RelacioCP(e1, e2);
					else if (aux.compareTo("PC") == 0)
					     a = new RelacioPC(e1, e2);
					else if (aux.compareTo("CsubC") == 0)
					     a = new RelacioCsubC(e1, e2);
					else a = new RelacioCsupC(e1, e2);
					break;
				case 8:
					if(b)graf_pes.AfegirAresta((ArestaPes)a);
					else graf_wiki.AfegirAresta((Relacio)a);
					break;
				case 9:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					if(b)graf_pes.BorrarVertex(e1);
					else graf_wiki.BorrarVertex(e1);
					break;
				case 10:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e2 = new Pagina(nom);
					else e2 = new Categoria(nom);
					
					if(b) graf_pes.BorrarAresta(e1, e2);
					else graf_wiki.BorrarAresta(e1, e2);
					break;
				case 11:
					System.out.print("Ordre: ");
					if(b)System.out.println(graf_pes.ordre());
					else System.out.println(graf_wiki.ordre());
					System.out.println("");
					
					break;
				case 12:
					System.out.print("Mode: ");
					if(b)System.out.println(graf_pes.mode());
					else System.out.println(graf_wiki.mode());
					System.out.println("");	
					break;
				case 13:
					System.out.print("Mida: ");
					if(b)System.out.println(graf_pes.mida());
					else System.out.println(graf_wiki.mida());
					System.out.println("");	
					break;
				case 14:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					
					System.out.println("Grau de " + e1.toString() + ":");
					if(b)System.out.println(graf_pes.grau(e1));
					else System.out.println(graf_wiki.grau(e1));
					System.out.println("");	
					break;
				case 15:
					x = sc.nextInt();
					System.out.println("Grau de " + x + ":");
					if(b)System.out.println(graf_pes.grau(x));
					else System.out.println(graf_wiki.grau(x));
					System.out.println("");	
					break;
				case 16:
					if(b)System.out.println(graf_pes.vertex(sc.nextInt()));
					else System.out.println(graf_wiki.vertex(sc.nextInt()));
					System.out.println("");	
					break;
				case 17:
					List<ElemWiki> el;
					if(b)el = graf_pes.ListaVertex();
					else el = graf_wiki.ListaVertex();
					System.out.println("Llista vertex:");
					for(ElemWiki e : el){
						System.out.println(e.toString());
					}
					System.out.println("");				
					break;
				case 18:
					if(b){
						x = sc.nextInt();
						List<Parell<Integer,Double>> ll = graf_pes.ArestesAdjacents(x);
						System.out.println("Arestes adjacents a: " + x);
						for(Parell<Integer,Double> xx : ll){
							System.out.println(xx.ObtenirPrimer() + " " + xx.ObtenirSegon());
						}
						System.out.println("");				
					}
					break;
				case 19:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					
					List<Parell<Integer,Double>> lp = graf_pes.ArestesAdjacents(e1);
					System.out.println("Arestes adjacents a: " + e1.toString());
					for(Parell<Integer,Double> xx : lp){
						System.out.println(xx.ObtenirPrimer() + " " + xx.ObtenirSegon());
					}
					System.out.println("");	
					
					break;
				case 20:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					
					List<ElemWiki> le = null;
					if(b) le = graf_pes.VertexAdjacents(e1);
					else le = graf_wiki.VertexAdjacents(e1);
					System.out.println("Vertex adjacents a: " + e1.toString());
					for(ElemWiki xx : le){
						System.out.println(xx.toString());
					}
					System.out.println("");
					break;		
				case 21:
					x = sc.nextInt();
					List<Integer> li = graf_pes.VertexAdjacents(x);
					System.out.println("Vertex adjacents a: " + x);
					for(int xx : li){
						System.out.println(xx);
					}
					System.out.println("");
					break;
				case 22:
					x= sc.nextInt();
					int x1 = sc.nextInt();
					System.out.println("Existeix aresta entre " + x + " i " + x1 + ": " + graf_pes.ExisteixAresta(x, x1));
					System.out.println("");
					break;
				case 23:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e2 = new Pagina(nom);
					else e2 = new Categoria(nom);
					
					if(b) System.out.println("Existeix aresta entre " + e1.toString() + " i " + e2.toString() + ": " + graf_pes.ExisteixAresta(e1, e2));
					else System.out.println("Existeix aresta entre " + e1.toString() + " i " + e2.toString() + ": " + graf_wiki.ExisteixAresta(e1, e2));
					System.out.println("");	
					break;
				case 24:
					x= sc.nextInt();
					int x2 = sc.nextInt();
					System.out.println("Pes aresta entre " + x + " i " + x2 + "= " + graf_pes.PesAresta(x, x2));
					System.out.println("");
					break;
				case 25:
					System.out.println("Arrels del graf: " + graf_wiki.Arrels());
					break;
				case 26:
					c = sc.nextBoolean();
					nom = sc.next();
					if(c) e1 = new Pagina(nom);
					else e1 = new Categoria(nom);
					List<Parell<ElemWiki,List<String>>> list = graf_wiki.ArestesAdjacentsDirigit(e1);
					System.out.println("Arestes adjacents a: " + e1.toString());
					for(Parell<ElemWiki,List<String>> p : list){
						System.out.print(p.ObtenirPrimer().toString() + " :");
						for(String ss : p.ObtenirSegon()){
							System.out.print(" " + ss + "," );
						}
						System.out.println();
					}
					System.out.println();
					break;
			}
			x = sc.nextInt();
		}
		sc.close();
		
	}
}
