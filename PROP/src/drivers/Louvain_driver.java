package drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;


public class Louvain_driver {
	static Graf<ElemWiki,ArestaPes,Double> graf_pes;
	static void menu_inici(){
		System.out.println("LOUVAIN");
		System.out.println("Creadora");
		System.out.println("1, Entrada: Louvain(Entrada En)");
		System.out.println("Modificadora");
		System.out.println("2, Entrada, Sortida: Cercar_Comunitats(Entrada En, Sortida So)");
		System.out.println("-1: sortir");
	}
	public static void main(String[] args)throws FileNotFoundException {
		// TODO Auto-generated method stub
		menu_inici();
		Scanner sc = new Scanner(System.in);
		int i = 0;
		graf_pes = new Graf<ElemWiki,ArestaPes,Double>(true);
		
		boolean c;
		String nom;
		ElemWiki e1;
		System.out.println("Vols afegir un nou vertex? (Si: != -2, No: -2)");
		i = sc.nextInt();
		
		while (i != -2) {
			System.out.println("Afegir Vertex: boolean, string");
			c = sc.nextBoolean();
			nom = sc.next();
			if(c) e1 = new Pagina(nom);
			else e1 = new Categoria(nom);
			graf_pes.AfegirVertex(e1);
			System.out.println("Vols afegir un nou vertex? (Si: != -2, No: -2)");
			i = sc.nextInt();
		}
		
		System.out.println("Vols afegir una nova aresta? (Si: != -2, No: -2)");
		i = sc.nextInt();
		while (i != -2) {
			System.out.println("Afegir Aresta: int (vertexO), double (pes), int(vertexD)");
			graf_pes.AfegirArestaNum(sc.nextInt(), sc.nextDouble(), sc.nextInt());
			System.out.println("Vols afegir una nova aresta? (Si: != -2, No: -2)");
			i = sc.nextInt();
		}
		
		Entrada En = new Entrada(graf_pes);
		Sortida So = new Sortida();
		Louvain Lo;
		while (i != -1){
			menu_inici();
			switch (i){
				case 1:
					System.out.println("Constructora");
					break;
				case 2:
					System.out.println("Algorisme");
					Lo = new Louvain(En,So);
					List<Graf> Grafs = new ArrayList<Graf>();
					Grafs = So.Comunitats_resultat();
					for (int z = 0; z < Grafs.size(); ++z) {
						List <ElemWiki> vertexs = new ArrayList <ElemWiki>();
						vertexs = Grafs.get(z).ListaVertex();
						for (int j = 0; j < vertexs.size(); ++j) {
							System.out.println(vertexs.get(j).NomNode());
						}
						System.out.println("::::::::::::::::");
					}
					break;					
			}
			i = sc.nextInt();
		}
		sc.close();
	}
}


