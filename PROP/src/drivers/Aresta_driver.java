package drivers;
import java.io.IOException;
import java.util.Scanner;

import domini.*;

public class Aresta_driver {
	static void menu_inici(){
		System.out.println("Creadores:");
		System.out.println("1 vertexO, vertexD, cont : Aresta(int vertexO ,int vertexD, int cont)");
		System.out.println("Modificadores:");
		System.out.println("2 c : void modificarPes(int c)");
		System.out.println("Consultores:");
		System.out.println("3: int pes()");
		System.out.println("4: int NodeDesti()");
		System.out.println("5: int NodeOrigen()");
		System.out.println("6: String toString()");
		System.out.println("-1: sortir");
	}
	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub
		Aresta<Integer,Integer> l = new Aresta<Integer,Integer>();
		menu_inici();
		Scanner s = new Scanner(System.in);
		int i = s.nextInt();
		while (i != -1){
			switch (i){
				case 1:
					l = new Aresta<Integer,Integer>(s.nextInt() ,s.nextInt(), s.nextInt());
					break;
				case 2:
					l.modificarPes(s.nextInt());
					break;
				case 3:
					System.out.println(l.pes());		
					break;
				case 4:
					System.out.println(l.NodeDesti());
					break;
				case 5:
					System.out.println(l.NodeOrigen());
					break;
				case 6:
					System.out.println(l.toString());
			}
			i = s.nextInt();
		}
		s.close();
	}
}
