package drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;

import domini.ArestaPes;
import domini.ArestaWiki;
import domini.Categoria;
import domini.ElemWiki;
import domini.Graf;
import domini.Pagina;
import domini.Parell;
import domini.damerau_levenshtein;


public class damerau_levenshtein_driver {
	static Graf<ElemWiki,ArestaPes,Double> graf_pes;
	static Graf<ElemWiki,ArestaWiki,String> graf_wiki;
	static damerau_levenshtein dl;
	static void menu_inici(){
		System.out.println("Driver de Damerau Levenshtein");
		System.out.println("Creadora:");
		System.out.println("1: g (graf d'entrada) : damerau_levenshtein(Graf<ElemWiki, ArestaWiki, String> g)");
		System.out.println("Modificadores:");
		System.out.println("2: void convert()");
		System.out.println("3: g : void modificar_graf_input(Graf<ElemWiki,ArestaWiki,String> g)");
		System.out.println("Consultores:");		
		System.out.println("4: Graf<ElemWiki,ArestaPes,Double> obtenir_graf()");
		System.out.println("5: Crear graf g input");
		System.out.println("6: imprimir graf output");
		System.out.println("-1: sortir");
	}
		
	public static void main(String[] args) throws FileNotFoundException {
		Scanner sc = new Scanner(System.in);
		menu_inici();
		int x = sc.nextInt();
		
		while(x != -1){
			switch (x) {
				case 1:
					dl = new damerau_levenshtein(graf_wiki);
					break;
				case 2:
					dl.convert();
					break;
				case 3:
					dl.modificar_graf_input(graf_wiki);
					break;
				case 4:
					graf_pes = dl.obtenir_graf();
					break;
				case 5:
					graf_wiki = new Graf<ElemWiki,ArestaWiki,String>(false);
					System.out.println("Entra els vertex en el format vertex, pg(boolean que indica 1 pagina 0 categoria)");
					System.out.println("quan acabis escriu -1");
					String v = sc.next();
					
					while(!v.equals("-1")){
						boolean pg = sc.nextBoolean();
						ElemWiki Vertex;
						if(pg)Vertex = new Pagina(v);
						else Vertex = new Categoria(v);
						graf_wiki.AfegirVertex(Vertex);
						v = sc.next();
					}
					System.out.println("Entra les arestes en format elem_origen(v,pg), nom_link , elem_desti(v,pg)");
					System.out.println("Quan acabis escriu -1");
					v = sc.next();
					while(!v.equals("-1")){
						boolean pgo = sc.nextBoolean();
						String link = sc.next();
						String nom_desti = sc.next();
						boolean pgd = sc.nextBoolean();
						ElemWiki origen;
						ElemWiki desti;
						if(pgo) origen = new Pagina(v);
						else origen = new Categoria(v);
						if(pgd) desti = new Pagina(nom_desti);
						else desti = new Categoria(nom_desti);
						graf_wiki.AfegirAresta(origen, link, desti);
						v = sc.next();
					}
					break;
					
				case 6:
					int n = graf_pes.ordre();
					boolean[][] visitades = new boolean[n][n];
					for(int i = 0; i < n; ++i) Arrays.fill(visitades[i],false);
					for(int i = 0; i < graf_pes.ordre();++i){
						List<Parell<Integer,Double>> l = graf_pes.ArestesAdjacents(i);
						for(int j = 0; j < l.size(); ++j ){
							
							if(!visitades[i][l.get(j).primer]) {
								visitades[i][l.get(j).primer] = true;
								visitades[l.get(j).primer][i] = true;
								System.out.println("Origen " + graf_pes.vertex(i).NomNode() + " pes " + l.get(j).segon + " desti " + graf_pes.vertex(l.get(j).primer));
								
							}
						
							
						}
					}
					break;

			}
			x = sc.nextInt();
		}
		sc.close();
		
	}
}
