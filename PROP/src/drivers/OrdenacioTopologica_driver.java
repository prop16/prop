package drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;

public class OrdenacioTopologica_driver {

	public static void main (String[] args) throws FileNotFoundException {
		
		Scanner sc = new Scanner(System.in);
		Graf<String,Aresta<String,String>,String> G = new Graf<String,Aresta<String,String>,String>(false);
		while (sc.hasNext()) {
			String s1 = sc.next();
			double pes = sc.nextDouble();
			String s2 = sc.next();
			G.AfegirAresta(s1, "t", s2);
		}
		sc.close();
		
		OrdenacioTopologica ot = new OrdenacioTopologica();
		try {
			ot.OrdenarGraf(G);
		}
		catch (Exception e) {
			System.out.println("Excepció -> " + e);
		}
		
		List<Integer> pr = ot.PrimeraRepresentacio();
		List<Integer> sr = ot.SegonaRepresentacio();
		
		for (int i = 0; i < pr.size(); i++) {
			try {
				System.out.println("PR: " + pr.get(i) + "; SR: " + G.vertex(sr.get(i)));
			}
			catch (IndexOutOfBoundsException e) {
				System.out.println("Excepció -> " + e);
			}
		}
	}
}
