package drivers;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class ControladorWiki_driver {
	static ControladorWiki CW;
	static void menu_inici(){
		System.out.println("1: public ControladorWiki()");
		System.out.println("2: public void Guardar(String path)");
		System.out.println("3: public void Carregar(String path)");
		System.out.println("4: public void AfegirAresta(String NodeOrigen, String TipusNodeOrigen, String TipusRelacio, String NodeDesti, String TipusNodeDesti)");
		System.out.println("5: public void EsborrarAresta(String NodeOrigen, String TipusNodeOrigen, String TipusRelacio, String NodeDesti, String TipusNodeDesti)");	
		System.out.println("6: public String ConsultarAresta_i(int i)");
		System.out.println("7: public int NombreArestes()");
		System.out.println("8: public void AfegirNode(String node, String tipusNode)");
		System.out.println("9: public void EliminarNode(String node, String tipusNode)");
		System.out.println("10: public String ConsultarNode_i(int i)");
		System.out.println("11: public int NombreNodes()");
		System.out.println("-1: sortir");
	}
	public static void main(String[] args)throws Exception {
		menu_inici();
		Scanner s = new Scanner(System.in);
		System.out.println("Opcio:");
		int x = s.nextInt();
		while (x != -1){
			switch (x){
				case 1:
					CW = new ControladorWiki();
					break;
				case 2:
					System.out.println("Introduir fitxer on guardar");
					try {
						CW.Guardar(s.next());
					}
					catch (Exception e) {
						throw e;
					}
					break;
				case 3:
					System.out.println("Introduir fitxer a carregar");
					try {
						CW.Carregar(s.next());
					}
					catch (Exception e) {
						throw e;
					}
					break;
				case 4:
					System.out.println("Introduir l'aresta a afegir: (NodeOrigen, TipusNodeOrigen, TipusRelacio, NodeDesti, TipusNodeDesti)");
					try {
						CW.AfegirAresta(s.next(), s.next(), s.next(), s.next(), s.next());
					}
					catch (Exception e) {
						throw e;
					}
					System.out.println("Aresta afegida");
					break;
				case 5:
					System.out.println("Introduir l'aresta a esborrar: (NodeOrigen, TipusNodeOrigen, TipusRelacio, NodeDesti, TipusNodeDesti)");
					try {
						CW.EsborrarAresta(s.next(), s.next(), s.next(), s.next(), s.next());
					}
					catch (Exception e) {
						throw e;
					}
					System.out.println("Aresta esborrada");
					break;
				case 6:
					System.out.println("Introduir numero d'aresta a visitar:");
					try {
						System.out.println(CW.ConsultarAresta_i(s.nextInt()));
					}
					catch (Exception e) {
						throw e;
					}
					break;
				case 7:
					System.out.println("Mida: " + CW.NombreArestes());
					break;
				case 8:
					System.out.println("Introduir nom i tipus de node");
					try {
						CW.AfegirNode(s.next(),s.next());
					}
					catch (Exception e) {
						throw e;
					}
					System.out.println("Node afegit!");
					break;
				case 9:
					System.out.println("Introduir nom i tipus de node");
					try {
						CW.EliminarNode(s.next(),s.next());
					}
					catch (Exception e) {
						throw e;
					}
					System.out.println("Node eliminat!");
					break;
				case 10:
					System.out.println("Introduir numero de node a visitar:");
					try {
						System.out.println(CW.ConsultarNode_i(s.nextInt()));
					}
					catch (IndexOutOfBoundsException e) {
						throw e;
					}
					break;
				case 11:
					System.out.println("Ordre: " + CW.NombreNodes());
					break;	

			}
			System.out.println("Opcio:");
			x = s.nextInt();
		}
		s.close();
	}
}
