package presentacio;

import javax.swing.*;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.graph.implementations.SingleGraph;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import domini.ControladorWiki;
import domini.Controlador_Algorismes;

public class CtrlPresentacioGrafPanell {
	
        ControladorWiki CW;
        Controlador_Algorismes CA;
        Graph g = new MultiGraph("actual");
        ArrayList<Graph> comunitats;
        int graf_actual;
        int colors = 12;
        public CtrlPresentacioGrafPanell (ControladorWiki cw, Controlador_Algorismes ca) {
        	    
        		comunitats = new ArrayList<Graph>();
                CW = cw;
                CA = ca;
                g.addAttribute("ui.stylesheet", Stylesheet);
                g.addAttribute("ui.quality");
        		g.addAttribute("ui.antialias");
        		g.setAutoCreate(true);
                g.setStrict(false);
                
                
        
        }
        
        public grafPanell getPanel(int i) {
            grafPanell g = new grafPanell(this,i);
            return g;
    }

        
        
        public void Intercanviar_comunitats(String node, String tipus,Integer comunitatO, Integer comunitatD){
        	Graph ComunitatO = comunitats.get(comunitatO - 1);
        	
        	Graph ComunitatD = comunitats.get(comunitatD - 1);
        	
        	ComunitatD.setStrict(false);
        	ComunitatD.setAutoCreate(true);
        	ComunitatO.removeNode(node+tipus);
        	
        	ComunitatD.addNode(node+tipus).addAttribute("ui.label", node);
        	ComunitatD.addNode(node+tipus).addAttribute("ui.class", tipus,"comunitat"+((comunitatD%colors)+1));
        	for(Edge e: g.getNode(node+tipus).getEachEnteringEdge()){
        		ArrayList<Integer> lc = e.getSourceNode().getAttribute("comunitat");
        		if( lc.contains(comunitatD) ){
        			ComunitatD.addEdge(e.getId(),e.getSourceNode().getId(),node+tipus,true);
        			ComunitatD.getEdge(e.getId()).addAttribute("ui.label",e.getAttribute("ui.label"));
        		}
        	}
        	for(Edge e: g.getNode(node+tipus).getEachLeavingEdge()){
        		ArrayList<Integer> lc = e.getTargetNode().getAttribute("comunitat");
        		if(lc.contains(comunitatD)){
        			ComunitatD.addEdge(e.getId(),node+tipus,e.getTargetNode().getId(),true);
        			ComunitatD.getEdge(e.getId()).addAttribute("ui.label",e.getAttribute("ui.label"));
        		}
        	}
        	
        	ArrayList<Integer> lc = g.getNode(node+tipus).getAttribute("comunitat");
        	if(lc.contains(comunitatO)){
        		lc.remove(comunitatO);
        	}
        	lc.add(comunitatD);
        	g.getNode(node+tipus).addAttribute("ui.label", node);
        	
        	
        	g.getNode(node+tipus).addAttribute("ui.label",g.getNode(node+tipus).getAttribute("ui.label")+" " +"c" + comunitatD.toString());
        	
        	if(g.getNode(node+tipus).getAttribute("comunitat",ArrayList.class).size() <= 1){
        		g.getNode(node+tipus).setAttribute("ui.class",g.getNode(node+tipus).getAttribute("tipus"),"comunitat"+((comunitatD%colors)+1));
        	
        	}
        	

        	
        	
        }
        
        public void visualitzar_node(String nom, String tipus){
        	ArrayList<Integer> lc = new ArrayList();
        	g.addNode(nom + tipus);
        	g.getNode(nom + tipus).addAttribute("ui.label", nom);
        	g.getNode(nom+tipus).addAttribute("nom", nom);
        	g.getNode(nom+tipus).addAttribute("tipus", tipus);
        	g.getNode(nom + tipus).addAttribute("ui.class",tipus);
        	if(!g.getNode(nom+tipus).hasAttribute("comunitat"))g.getNode(nom+tipus).addAttribute("comunitat", lc);
        	if(g.getNodeCount() >= 40){
    			g.removeAttribute("ui.stylesheet");
    			g.addAttribute("ui.stylesheet",StylesheetMoltsNodes);
    		}
        }
        
        public void visualitzar_aresta(String nomO, String tipusO, String relacio, String nomD, String tipusD){
        	ArrayList<Integer> lco = new ArrayList();
        	ArrayList<Integer> lcd = new ArrayList();
        	g.addEdge(nomO+tipusO+nomD+tipusD+relacio, nomO+tipusO, nomD+tipusD,true);
        	g.getNode(nomO+tipusO).addAttribute("ui.label", nomO);
        	g.getNode(nomO+tipusO).addAttribute("nom", nomO);
        	g.getNode(nomO+tipusO).addAttribute("tipus", tipusO);
        	g.getNode(nomO+tipusO).addAttribute("ui.class",tipusO);
        	
        	g.getNode(nomD+tipusD).addAttribute("ui.label", nomD);
        	g.getNode(nomD+tipusD).addAttribute("nom", nomD);
        	g.getNode(nomD+tipusD).addAttribute("tipus", tipusD);
        	g.getNode(nomD+tipusD).addAttribute("ui.class",tipusD);
        	g.getEdge(nomO+tipusO+nomD+tipusD+relacio).addAttribute("ui.label",relacio);
        	if(!g.getNode(nomO+tipusO).hasAttribute("comunitat"))g.getNode(nomO+tipusO).addAttribute("comunitat", lco);
        	if(!g.getNode(nomD+tipusD).hasAttribute("comunitat"))g.getNode(nomD+tipusD).addAttribute("comunitat", lcd);
        	if(g.getNodeCount() >= 40){
    			g.removeAttribute("ui.stylesheet");
    			g.addAttribute("ui.stylesheet",StylesheetMoltsNodes);
    		}
        	
        }
        
		public void No_visualitzar_aresta(String nomO, String tipusO,String relacio,String nomD, String tipusD){
		        	
	    	g.removeEdge(nomO+tipusO+nomD+tipusD+relacio);
	    	
	    	if(g.getNodeCount() < 40){
				g.removeAttribute("ui.stylesheet");
				g.addAttribute("ui.stylesheet",Stylesheet);
			}
		        	
		}
		 
		public void No_visualitzar_node(String nomO, String tipusO){
	        	
		    	g.removeNode(nomO+tipusO);
		    	
		    	if(g.getNodeCount() < 40){
					g.removeAttribute("ui.stylesheet");
					g.addAttribute("ui.stylesheet",Stylesheet);
				}
			        	
			}
			
                public void Modificar_aresta(String nomO, String tipusO,String relacio,String nomD, String tipusD) {
                        
                        if (relacio.equals("CsubC")) {
                                g.getEdge(nomO+tipusO+nomD+tipusD+relacio).setAttribute("ui.label","CsupC");
                        }
                        else {
                                g.getEdge(nomO+tipusO+nomD+tipusD+relacio).setAttribute("ui.label","CsubC");
                        }
                
                }
		
		public void Canviar_nom_node(String node, String tipus, String nom){
			
			g.addNode(nom+tipus);
			g.getNode(nom+tipus).addAttribute("ui.label", nom);
			for(Edge e : g.getNode(node+tipus).getEachLeavingEdge()){
				g.addEdge(nom+tipus+e.getTargetNode().getId()+e.getAttribute("ui.label"),nom+tipus,e.getTargetNode().getId(),true);
				g.getEdge(nom+tipus+e.getTargetNode().getId()+e.getAttribute("ui.label")).addAttribute("ui.label", e.getAttribute("ui.label"));
			}
			
			for(Edge e : g.getNode(node+tipus).getEachEnteringEdge()){
				g.addEdge(e.getSourceNode().getId()+nom+tipus,e.getTargetNode().getId()+e.getAttribute("ui.label"),nom+tipus,true);
				g.getEdge(e.getSourceNode().getId()+nom+tipus+e.getAttribute("ui.label")).addAttribute("ui.label", e.getAttribute("ui.label"));
			}
			g.getNode(nom+tipus).addAttribute("comunitat",g.getNode(node+tipus).getAttribute("comunitat"));
			g.getNode(nom+tipus).addAttribute("nom", nom);
        	g.getNode(nom+tipus).addAttribute("tipus", tipus);
			g.getNode(nom+tipus).addAttribute("ui.class",g.getNode(node+tipus).getAttribute("ui.class"));
			g.removeNode(node+tipus);
		}
        
		void Pintar_node_comunitat(Integer ncom, String node, String tipus){

			ArrayList<Integer> lc = g.getNode(node+tipus).getAttribute("comunitat");

			lc.add(ncom);
			
			g.getNode(node+tipus).addAttribute("ui.label",g.getNode(node+tipus).getAttribute("ui.label")+" " +"c" + ncom.toString());
        	

			for(Edge e: g.getNode(node+tipus).getEachEnteringEdge()){
        		ArrayList<Integer> lca = e.getSourceNode().getAttribute("comunitat");
        		if( lca.contains(ncom) ){

        			comunitats.get(ncom-1).addEdge(e.getId(),e.getSourceNode().getId(),node+tipus,true);
        			comunitats.get(ncom-1).getNode(e.getSourceNode().getId()).addAttribute("ui.label",e.getSourceNode().getAttribute("nom") );
        			comunitats.get(ncom-1).getNode(e.getSourceNode().getId()).addAttribute("ui.class",e.getSourceNode().getAttribute("ui.class") );
        			comunitats.get(ncom-1).getEdge(e.getId()).addAttribute("ui.label",e.getAttribute("ui.label"));
        		}
        	}
        	for(Edge e: g.getNode(node+tipus).getEachLeavingEdge()){
        		ArrayList<Integer> lca = e.getTargetNode().getAttribute("comunitat");
        		if(lca.contains(ncom)){
        			comunitats.get(ncom-1).addEdge(e.getId(),node+tipus,e.getTargetNode().getId(),true);
        			comunitats.get(ncom-1).getNode(e.getTargetNode().getId()).addAttribute("ui.label",e.getTargetNode().getAttribute("nom") );
        			comunitats.get(ncom-1).getNode(e.getTargetNode().getId()).addAttribute("ui.class",e.getTargetNode().getAttribute("ui.class") );
        			comunitats.get(ncom-1).getEdge(e.getId()).addAttribute("ui.label",e.getAttribute("ui.label"));

        		}
        	}
        	
        	
        	if(g.getNode(node+tipus).getAttribute("comunitat",ArrayList.class).size() > 1){
        		g.getNode(node+tipus).addAttribute("ui.class", "overlaping");
        		
        	
        	}
        	else {
        		g.getNode(node+tipus).setAttribute("ui.class",g.getNode(node+tipus).getAttribute("tipus"),"comunitat"+((ncom%colors)+1));
        		
        	}
        	comunitats.get(ncom-1).addNode(node+tipus);
        	Node n = comunitats.get(ncom-1).getNode(node+tipus);
        	n.addAttribute("ui.label",node);
        	n.addAttribute("ui.class",g.getNode(node+tipus).getAttribute("ui.class") );
        	
		}
		
		void inicialitzar_comunitats(int nre_comunitats){
			
			for(Node n : g.getEachNode()){
				ArrayList<Integer> lc= new  ArrayList<Integer>();

				n.setAttribute("ui.label", n.getAttribute("nom"));
				n.setAttribute("ui.class", n.getAttribute("tipus"));
				n.setAttribute("comunitat",lc);
				
			}
			comunitats = new ArrayList<Graph>();
			for(Integer i = 0; i < nre_comunitats;++i){
				Graph gc = new MultiGraph(i.toString());
				gc.addAttribute("ui.stylesheet", Stylesheet);
				gc.addAttribute("ui.quality");
        		gc.addAttribute("ui.antialias");
        		gc.setAutoCreate(true);
                gc.setStrict(false);
				comunitats.add(gc);
				
			}
		}
		
		void crear_comunitat(){
			Graph gc = new MultiGraph(((Integer)(comunitats.size())).toString());
			gc.addAttribute("ui.stylesheet", Stylesheet);
			gc.addAttribute("ui.quality");
    		gc.addAttribute("ui.antialias");
    		gc.setAutoCreate(true);
            gc.setStrict(false);
			comunitats.add(gc);
		}
		
        public void crear_graf(){
        	

        	g = new MultiGraph("actual");
        	g.addAttribute("ui.stylesheet", Stylesheet);
            g.addAttribute("ui.quality");
    		g.addAttribute("ui.antialias");
    		g.setAutoCreate(true);
            g.setStrict(false);
            
        	int linies_llegides = 0;
                
                
        	String[] bloc = CW.CarregarPrimerBlocLinies().split("\n");
                if (CW.mida() > 0) {
                        linies_llegides+=bloc.length;
                        for(int i = 0; i < bloc.length; ++i){
                                String[] linia = bloc[i].split(" ");
                                String nomO = linia[0];
                                String tipusO = linia[1];
                                String relacio = linia[2];
                                String nomD = linia[3];
                                String tipusD = linia[4];
                                visualitzar_aresta(nomO, tipusO, relacio, nomD, tipusD);
                                if(g.getNodeCount() >= 40){
                                        g.removeAttribute("ui.stylesheet");
                                        g.addAttribute("ui.stylesheet",StylesheetMoltsNodes);
                                }
                        }
                        while(linies_llegides < CW.mida()){
                                bloc = CW.CarregarBlocLiniesNext().split("\n");
                        linies_llegides+=bloc.length;
                                for(int i = 0; i < bloc.length; ++i){
                                        String[] linia = bloc[i].split(" ");
                                        String nomO = linia[0];
                                        String tipusO = linia[1];
                                        String relacio = linia[2];
                                        String nomD = linia[3];
                                        String tipusD = linia[4];
                                        visualitzar_aresta(nomO, tipusO, relacio, nomD, tipusD);
                                        if(g.getNodeCount() >= 40){
                                                g.removeAttribute("ui.stylesheet");
                                                g.addAttribute("ui.stylesheet",StylesheetMoltsNodes);
                                        }
                                }
                        }
        	}
        	
        	linies_llegides = 0;
        	bloc = CW.CarregarPrimerBlocCategories().split("\n");

        	linies_llegides+=bloc.length;
        	for(int i = 0; i < bloc.length; ++i){
        		
        		if(!bloc[i].isEmpty()){
        			visualitzar_node(bloc[i],"cat");

        		}
        		if(g.getNodeCount() >= 40){
        			g.removeAttribute("ui.stylesheet");
        			g.addAttribute("ui.stylesheet",StylesheetMoltsNodes);
        		}
        	}
        	while(linies_llegides < CW.ordreCats()){
        		bloc = CW.CarregarBlocCategoriesNext().split("\n");

            	linies_llegides+=bloc.length;
            	for(int i = 0; i < bloc.length; ++i){

            		if(!bloc[i].isEmpty()){
            			visualitzar_node(bloc[i],"cat");

            		}
            		if(g.getNodeCount() >= 40){
            			g.removeAttribute("ui.stylesheet");
            			g.addAttribute("ui.stylesheet",StylesheetMoltsNodes);
            		}
            	}
        	}
        	
        	linies_llegides = 0;
        	bloc = CW.CarregarPrimerBlocPagines().split("\n");

        	linies_llegides+=bloc.length;
        	for(int i = 0; i < bloc.length; ++i){

        		if(!bloc[i].isEmpty()){
        			visualitzar_node(bloc[i],"page");

        		}
        		if(g.getNodeCount() >= 40){
        			g.removeAttribute("ui.stylesheet");
        			g.addAttribute("ui.stylesheet",StylesheetMoltsNodes);
        		}
        	}
        	while(linies_llegides < CW.ordrePags()){
        		bloc = CW.CarregarBlocPaginesNext().split("\n");

            	linies_llegides+=bloc.length;
            	for(int i = 0; i < bloc.length; ++i){

            		if(!bloc[i].isEmpty()){
            			visualitzar_node(bloc[i],"page");

            		}
            		if(g.getNodeCount() >= 40){
            			g.removeAttribute("ui.stylesheet");
            			g.addAttribute("ui.stylesheet",StylesheetMoltsNodes);
            		}
            	}
        	}
        	
        }
        
        public Graph obtenir_graf(){
        	graf_actual = 0;
        	return g;
        	
        }
        
        public Graph obtenir_comunitat(int n){
        	graf_actual = n;
        	return comunitats.get(n-1);
        	
        	
        }
        
        public ArrayList<String> comunitats(){
        	ArrayList<String> l = new ArrayList<String>();
        	int n = CA.ConsultarSortida().Comunitats_resultat().size();
        	for(int i = 0 ; i < n ; ++i) l.add("Comunitat "+(i+1));
        	
        	return l;
        }
        
        protected String stringcolors = 
        		"node.comunitat1{"+
        		"	fill-color: #FE5500;" +
        		"}"+
        		"node.comunitat2{"+
        		"	fill-color: #9AC158;" +
        		"}"+
        		"node.comunitat3{"+
        		"	fill-color: #EF85B3;" +
        		"}"+
        		"node.comunitat4{"+
        		"	fill-color: #0020B0;" +
        		"}"+
        		"node.comunitat5{"+
        		"	fill-color: #FABA00;" +
        		"}"+
        		"node.comunitat6{"+
        		"	fill-color: #D6100F;" +
        		"}"+
        		"node.comunitat7{"+
        		"	fill-color: #005600;" +
        		"}"+
        		"node.comunitat8{"+
        		"	fill-color: #4B3A8D;" +
        		"}"+
        		"node.comunitat9{"+
        		"	fill-color: white;" +
        		"}"+
        		"node.comunitat10{"+
        		"	fill-color: #B5B4B4;" +
        		"}"+
        		"node.comunitat11{"+
        		"	fill-color: #FFFE82;" +
        		"}"+
        		"node.comunitat12{"+
        		"	fill-color: #5CB6F0;" +
        		"}"+
        		"node.overlaping{"+
        		"	shape: cross;" +
        		"}"
        		;
       
        
        protected String Stylesheet =
                "node {" +
                "   fill-color: black;" +
                "   size: 10px;"+
                "   z-index: 0;"+
                "   text-mode: normal;"+
                "   text-background-color: white;"+
                "   text-background-mode: rounded-box;"+
                "   text-alignment: at-left;"+
                "   text-size: 12;"+
                
                "}"+
                "edge {"+
                "   shape: line;"+
                "   fill-color: black;"+
                "   text-alignment: along;"+
                "   text-background-color: white;"+
                "   text-background-mode: plain;"+
                "   arrow-shape: arrow;"+
                "   text-size: 12;"+
                "}"+
                "node.cat {"+
                "   shape: rounded-box;"+
                "   size: 15px;"+
                "}"+ stringcolors;
        
        protected String StylesheetMoltsNodes =
                "node {" +
                "   fill-color: black;" +
                "   size: 3px;"+
                "   z-index: 0;"+
                "   text-mode: normal;"+
                "   text-background-color: white;"+
                "   text-background-mode: plain;"+
                "   text-alignment: at-left;"+
                "   text-visibility-mode: under-zoom;"+
                "   text-visibility: 0.4;"+
                "}"+
                "edge {"+
                "   shape: line;"+
                "   fill-color: black;"+
                "   text-alignment: along;"+
                "   text-background-color: white;"+
                "   text-background-mode: plain;"+
                "   arrow-shape: none;"+
                "   text-visibility-mode: under-zoom;"+
                "   text-visibility: 0.4;"+
                "}"+
                "node.cat {"+
                "   shape: rounded-box;"+
                "   size: 5px;"+
                "}" + stringcolors;
        
}

