package presentacio;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import domini.ControladorWiki;
import domini.Controlador_Algorismes;


public class CtrlPresentacioFitxer {

    ControladorWiki CW;
    Controlador_Algorismes CA;
    CtrlPresentacioGrafPanell CPGP;
    
    public CtrlPresentacioFitxer (ControladorWiki cw, Controlador_Algorismes ca, CtrlPresentacioGrafPanell cpgp) {
            CW = cw;
            CA = ca;
            CPGP = cpgp;
    }
    
    public Fitxer getPanel() {
		Fitxer f = new Fitxer(this);
		return f;
    }
    
    public void importarArestes(String path) throws Exception{
    	CW.CarregarArestes(path);
		
    }
    
    public void exportarArestes(String path) throws Exception{
    	CW.GuardarArestes(path);
    }
    
    public void importarVertexs(String path) throws Exception{
    	CW.CarregarVertexs(path);
		
    }
    
    public void exportarVertexs(String path) throws Exception{
    	CW.GuardarVertexs(path);
    }
    
    public void exportar_comunitat_arestes(String path,int ncom) throws Exception{
        CA.exportarArestes(ncom, path, CW.ctrlpersistencia());
    }
    
    public void exportar_comunitat_vertexs(String path, int ncom) throws Exception{
        CA.exportarNodes(ncom, path, CW.ctrlpersistencia());
    }
    
   public void crear_graph(){
	   CPGP.crear_graf();
   }
    
    public ArrayList<String> comunitats(){
    	ArrayList<String> l = new ArrayList<String>();
    	int n = CA.ConsultarSortida().Comunitats_resultat().size();
    	for(int i = 0 ; i < n ; ++i) l.add("Comunitat "+(i+1));
    	
    	return l;
    }

}
