package presentacio;

import javax.swing.*;

import domini.DadesIncorrectesException;

import java.awt.*;
import java.awt.event.*;
import java.io.File;


public class Fitxer extends UnaList {

	JButton outputA, outputV; //exportar
	File fileopen;
	File filesave;
	JOptionPane overw;
	CtrlPresentacioFitxer cpf;
	
  public Fitxer(CtrlPresentacioFitxer CPF) {  
      cpf = CPF;
	  final JPanel j = this;
	  buto1 = new JButton();
	  buto1.setText("Importar Relacions");
	  buto1.setMinimumSize(new Dimension(200,25));
	  buto1.setMaximumSize(new Dimension(300,25));
	  add(buto1);
	  
	  outputA = new JButton();
	  outputA.setText("Exportar Relacions");
	  outputA.setMinimumSize(new Dimension(200,25));
	  outputA.setMaximumSize(new Dimension(300,25));
	  add(outputA);
	  
	  buto2 = new JButton();
	  buto2.setText("Importar Pagines/Categories");
	  buto2.setMinimumSize(new Dimension(200,25));
	  buto2.setMaximumSize(new Dimension(300,25));
	  add(buto2);
	  
	  outputV = new JButton();
	  outputV.setText("Exportar Pagines/Categories");
	  outputV.setMinimumSize(new Dimension(200,25));
	  outputV.setMaximumSize(new Dimension(300,25));
	  add(outputV);
	  
	  JFileChooser fc = new JFileChooser();
	  
	  text1 = new JTextField();
	  text1.setEnabled(false);
	  text1.setAutoscrolls(true);
	  text1.setText("Ruta/al/fitxer");
	  text1.setMinimumSize(new Dimension(400,25));
	  text1.setMaximumSize(new Dimension(600,25));
	  add(text1);
	  
	  etiq1 = new JLabel("Tria la comunitat a exportar");
	  etiq1.setMinimumSize(new Dimension(200,25));
	  etiq1.setMaximumSize(new Dimension(300,25));
	  add(etiq1);
	  
	  etiq2 = new JLabel();
	  etiq2.setForeground(Color.red);
	  add(etiq2);
	  
	  String[] params = new String[cpf.comunitats().size()+1];
	  params[0] = "Graf Sencer";
	  for(int i = 1; i < cpf.comunitats().size()+1; ++i) params[i] = cpf.comunitats().get(i-1);
	  llista1 = new JList(params);
	  scroll1 = new JScrollPane(llista1);
	  scroll1.setMinimumSize(new Dimension(300,300));
	  scroll1.setPreferredSize(new Dimension(300, 700));
	  scroll1.setMaximumSize(new Dimension(400,1000));
	  add(scroll1);
	  
        buto1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                        JFileChooser fc = new JFileChooser();
                        int returnVal = fc.showOpenDialog(j);
                        if (returnVal == JFileChooser.APPROVE_OPTION) {
                                fileopen = fc.getSelectedFile();
                                text1.setText(fileopen.getAbsolutePath());
                                try {
                                        cpf.importarArestes(fileopen.getAbsolutePath());
                                        cpf.crear_graph();
                                        etiq2.setForeground(Color.green);
                                        etiq2.setText("Importació realitzada satisfactòriament");
                                } 
                                catch (DadesIncorrectesException e1) {
                                        etiq2.setForeground(Color.red);
                                        etiq2.setText(e1.getMessage());
                                } 
                                catch (Exception e1) {
                                        etiq2.setForeground(Color.red);
                                        etiq2.setText(e1.getMessage());
                                }
                        } 
                }
        });
	  
        outputA.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                        if (llista1.getSelectedIndex() == -1) etiq2.setText("No has seleccionat cap graf/comunitat");
                        else {
                                etiq2.setText("");
                                JFileChooser fc = new JFileChooser();
                                int returnVal = fc.showSaveDialog(j);
                                if (returnVal == JFileChooser.APPROVE_OPTION) filesave = fc.getSelectedFile();

                                int n = JOptionPane.NO_OPTION;
                                boolean exist = true;
                                while(n == JOptionPane.NO_OPTION && returnVal == JFileChooser.APPROVE_OPTION && exist) {
                                        exist = false;
                                        if(filesave.exists()){
                                                exist = true;
                                                overw = new JOptionPane();
                                                Object[] opcions = {"Si","No","Cancelar"};
                                                n = overw.showOptionDialog(j, "Vols sobrescriure el fitxer " + filesave.getName(),
			    			  "Alerta",
			    			  JOptionPane.YES_NO_CANCEL_OPTION,
			    			  JOptionPane.WARNING_MESSAGE,
			    			  null,
			    			  opcions,
			    			  opcions[0]);   
			    			  fc.setCurrentDirectory(filesave);
			    			  if(n == JOptionPane.NO_OPTION){
                                                        returnVal = fc.showSaveDialog(j);
                                                        if (returnVal == JFileChooser.APPROVE_OPTION) filesave = fc.getSelectedFile();
                                                  }
                                        }
                                }

                                if(returnVal != JFileChooser.CANCEL_OPTION && n != JOptionPane.CANCEL_OPTION){
                                        if(llista1.getSelectedValue().equals("Graf Sencer"))
                                                try {
                                                        cpf.exportarArestes(filesave.getAbsolutePath());
                                                        etiq2.setForeground(Color.green);
                                                        etiq2.setText("Exportació realitzada satisfactòriament");
                                                } catch (Exception e1) {
                                                        etiq2.setForeground(Color.red);
                                                        etiq2.setText(e1.getMessage());
                                                }
                                        else{
                                                int ncom = llista1.getSelectedIndex();
                                                try {
                                                        cpf.exportar_comunitat_arestes(filesave.getAbsolutePath(), ncom);
                                                        etiq2.setForeground(Color.green);
                                                        etiq2.setText("Exportació realitzada satisfactòriament");
                                                } catch (Exception e1) {
                                                        etiq2.setForeground(Color.red);
                                                        etiq2.setText(e1.getMessage());
                                                }
                                        }
                                }
		      }
		      
		      
		      
	            }
	      });
	      
        buto2.addActionListener(new ActionListener() {
		  public void actionPerformed(ActionEvent e) {
			  JFileChooser fc = new JFileChooser();
			  int returnVal = fc.showOpenDialog(j);
			  if (returnVal == JFileChooser.APPROVE_OPTION) {
		          fileopen = fc.getSelectedFile();
		          text1.setText(fileopen.getAbsolutePath());
		          try {
					cpf.importarVertexs(fileopen.getAbsolutePath());
					cpf.crear_graph();
					etiq2.setForeground(Color.green);
                                        etiq2.setText("Importació realitzada satisfactòriament");
				} catch (DadesIncorrectesException e1) {
					etiq2.setForeground(Color.red);
					etiq2.setText(e1.getMessage());
				} catch (Exception e1) {
					etiq2.setForeground(Color.red);
					etiq2.setText(e1.getMessage());
				}

		      } 
	            }
	      });
	  
        outputV.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                        if (llista1.getSelectedIndex() == -1) etiq2.setText("No has seleccionat cap graf/comunitat");
                        else {
                                etiq2.setText("");
                                JFileChooser fc = new JFileChooser();
                                int returnVal = fc.showSaveDialog(j);
                                if (returnVal == JFileChooser.APPROVE_OPTION) filesave = fc.getSelectedFile();

                                int n = JOptionPane.NO_OPTION;
                                boolean exist = true;
                                while(n == JOptionPane.NO_OPTION && returnVal == JFileChooser.APPROVE_OPTION && exist) {
                                        exist = false;
                                        if(filesave.exists()){
                                                exist = true;
                                                overw = new JOptionPane();
                                                Object[] opcions = {"Si","No","Cancelar"};
                                                n = overw.showOptionDialog(j, "Vols sobrescriure el fitxer " + filesave.getName(),
			    			  "Alerta",
			    			  JOptionPane.YES_NO_CANCEL_OPTION,
			    			  JOptionPane.WARNING_MESSAGE,
			    			  null,
			    			  opcions,
			    			  opcions[0]);   
			    			  fc.setCurrentDirectory(filesave);
			    			  if(n == JOptionPane.NO_OPTION){
                                                        returnVal = fc.showSaveDialog(j);
                                                        if (returnVal == JFileChooser.APPROVE_OPTION) filesave = fc.getSelectedFile();
                                                  }
                                        }
                                }

                                if(returnVal != JFileChooser.CANCEL_OPTION && n != JOptionPane.CANCEL_OPTION){
                                        if(llista1.getSelectedValue().equals("Graf Sencer"))
                                                try {
                                                        cpf.exportarVertexs(filesave.getAbsolutePath());
                                                        etiq2.setForeground(Color.green);
                                                        etiq2.setText("Exportació realitzada satisfactòriament");
                                                } catch (Exception e1) {
                                                        etiq2.setForeground(Color.red);
                                                        etiq2.setText(e1.getMessage());
                                                }
                                        else{
                                                int ncom = llista1.getSelectedIndex();
                                                try {
                                                        cpf.exportar_comunitat_vertexs(filesave.getAbsolutePath(), ncom);
                                                        etiq2.setForeground(Color.green);
                                                        etiq2.setText("Exportació realitzada satisfactòriament");
                                                } catch (Exception e1) {
                                                        etiq2.setForeground(Color.red);
                                                        etiq2.setText(e1.getMessage());
                                                }
                                        }
                                }
		      }
		      
		      
		      
	            }
	      });
    
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(10,50,100)
                        .addGroup(layout.createParallelGroup()
                                .addComponent(etiq1)
                                .addComponent(scroll1))
                        .addGap(10,50,100)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(buto1)
                                        .addComponent(outputA)
                                        .addComponent(buto2)
                                        .addComponent(outputV))
                                .addComponent(text1)
                                .addComponent(etiq2))
                        .addGap(10,50,100)
                        .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(70,90,110)
                        .addComponent(etiq1)
                        .addGap(10,20,30)
                        .addGroup(layout.createParallelGroup()
                                .addComponent(scroll1)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(50,100,350)
                                        .addComponent(buto1)
                                        .addGap(10,20,30)
                                        .addComponent(outputA)
                                        .addGap(10,20,30)
                                        .addComponent(buto2)
                                        .addGap(10,20,30)
                                        .addComponent(outputV)
                                        .addGap(10,20,30)
                                        .addComponent(text1)
                                        .addGap(10,20,30)
                                        .addComponent(etiq2)
                                        .addGap(50,100,350)))
                        .addGap(40,60,80)
                        .addContainerGap())

        );
  }
  


}


