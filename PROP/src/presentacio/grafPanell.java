package presentacio;

import org.graphstream.graph.*;
import org.graphstream.graph.implementations.MultiGraph;


import org.graphstream.ui.view.View;
import org.graphstream.ui.view.Viewer;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.util.Hashtable;


public class grafPanell extends JPanel {
	Graph g;
	grafPanell jo = this;
	Graph graf;
	JPanel grafpanell;
	JSlider sl;
	Viewer viewer;
	View view;
	CtrlPresentacioGrafPanell cpgp;
	
	public grafPanell(CtrlPresentacioGrafPanell CPGP, int i){

		cpgp = CPGP;
		if(i == 0){
			g = cpgp.obtenir_graf();

		}
		else {
			g = cpgp.obtenir_comunitat(i);

		}
		sl = new JSlider();
		JPanel panellgraf = new JPanel();
		System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		
		Viewer viewer = new Viewer(g, Viewer.ThreadingModel.GRAPH_IN_GUI_THREAD);
		View view = viewer.addDefaultView(false);
		add((JPanel)view);
		
		view.getCamera().resetView();
		view.getCamera().setAutoFitView(true);
		viewer.enableAutoLayout();

		sl.setMaximum(100);
		sl.setMinimum(0);
		sl.setValue(0);
		sl.setMajorTickSpacing(10);
		sl.setPaintTicks(true);
		Hashtable labelTable = new Hashtable();
		labelTable.put( new Integer( 0 ), new JLabel("0%") );
		labelTable.put( new Integer( 100 ), new JLabel("100%") );
		sl.setLabelTable(labelTable);
		sl.setPaintLabels(true);
		sl.addChangeListener(new zoom_slider(view));
		
		
		
		Zoom_pan_listener zpl = new Zoom_pan_listener(view);
		((JPanel)(view)).addMouseMotionListener(zpl);
		((JPanel)view).addMouseWheelListener(zpl);
		view.addMouseListener(zpl);
		
	

		  

		
		add(sl);
		
		GroupLayout graflayout = new GroupLayout(this);
		setLayout(graflayout);
		graflayout.setHorizontalGroup(
			graflayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(graflayout.createSequentialGroup()
				.addGap(10, 10, 20)
				.addGroup(graflayout.createParallelGroup(GroupLayout.Alignment.LEADING)
					.addComponent(((JPanel)(view)))
					.addComponent(sl))
				.addGap(10, 10, 20)
				.addContainerGap())
		);
		graflayout.setVerticalGroup(
			graflayout.createParallelGroup(GroupLayout.Alignment.LEADING)
			.addGroup(graflayout.createSequentialGroup()
			.addGap(35, 35, 40)
			.addGroup(graflayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				.addComponent((JPanel)view))
			.addGap(10,15,20)
			.addGroup(graflayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
				.addComponent(sl))
			.addGap(10,10,20)
			.addContainerGap())
		);
		
		
	}
	
	private class zoom_slider implements ChangeListener{
		private View view1;
		
		
		public zoom_slider(View v){
			view1 = v;
			
		}

		public void stateChanged(ChangeEvent arg0) {
			view1.getCamera().setViewPercent(1.0 - (double)((JSlider) arg0.getSource()).getValue()/100.0);
			view1.getCamera().setViewPercent(1.0 - (double)sl.getValue()/100.0);
	
		}
		
	}
	private class Zoom_pan_listener implements MouseListener, MouseMotionListener, MouseWheelListener{
		private int max = 100;
		private int min = 0;
		int zoom = 0;
		private View view;
		private Point start;
		private Point end;

		public Zoom_pan_listener(View v){

			view = v;
		}
		public void mouseWheelMoved(MouseWheelEvent arg0) {
			int rotation = arg0.getWheelRotation();
			if(rotation < 0 && zoom < max  ){
				++zoom;
				view.getCamera().setViewPercent(1.0 - (double)zoom/100.0);
			}
			
			else if (rotation > 0 && zoom > min){
				--zoom;
				view.getCamera().setViewPercent(1.0 - (double)zoom/100.0);
			}
			
		}

		
		public void mouseDragged(MouseEvent arg0) {
			// TODO Auto-generated method stub
			end = arg0.getPoint();
			double sx = view.getCamera().transformPxToGu(start.getX(), start.getY()).x;
			double sy = view.getCamera().transformPxToGu(start.getX(), start.getY()).y;
			double ex = view.getCamera().transformPxToGu(end.getX(), end.getY()).x;
			double ey = view.getCamera().transformPxToGu(end.getX(), end.getY()).y;
			double dx = ex - sx;
			double dy = ey - sy;
		
			double x = view.getCamera().getViewCenter().x;
			double y = view.getCamera().getViewCenter().y;
			double z = view.getCamera().getViewCenter().z;

			view.getCamera().setViewCenter(x - dx, y - dy, z);

			start = end;
			end = null;
			
		}

		@Override
		public void mouseMoved(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			start = arg0.getPoint();
			end = null;
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
}
	
		
