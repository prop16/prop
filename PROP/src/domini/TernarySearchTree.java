package domini;
import java.util.ArrayList;

/** class TSTNode **/
class TSTNode<T>{
	char data;
	T value;
	boolean isEnd;
	TSTNode<T> left, middle, right;

	/** Constructor **/
	public TSTNode(char d){
		data = d;
		isEnd = false;
		left = null;
		middle = null;
		right = null;
	}        
}



/** class TernarySearchTree **/

class TernarySearchTree<T>{

	private TSTNode<T> root;
	private ArrayList<String> al;

	/** Constructor **/
	public TernarySearchTree(){
		root = null;
	}
	public TernarySearchTree(TernarySearchTree t){
		root = copy_node(t.root);
		//al = new ArrayList<Integer>(t.al);
	}
	private TSTNode copy_node(TSTNode n){
		TSTNode ret = new TSTNode(n.data);
		ret.isEnd = n.isEnd;
		ret.value = n.value;
		if(n.left != null) ret.left = copy_node(n.left);
		if(n.middle != null) ret.middle = copy_node(n.middle);
		if(n.right != null) ret.right = copy_node(n.right);
		
		return ret;
	}

	/** function to check if empty **/
	public boolean isEmpty(){
		return root == null;
	}

	/** function to clear **/
	public void makeEmpty(){
		root = null;
	}

	/** function to insert a key with value t **/
	public void insert(String key, T t){
		root = insert(root, key.toCharArray(), 0, t);
	}

	/** function to insert a key with value t **/
	public TSTNode insert(TSTNode r, char[] key, int ptr, T t){
		if (r == null)
		r = new TSTNode(key[ptr]);
		if (key[ptr] < r.data)                r.left = insert(r.left, key, ptr,t);
		else if (key[ptr] > r.data)           r.right = insert(r.right, key, ptr,t);
		else if (ptr < key.length - 1)        r.middle = insert(r.middle, key, ptr + 1,t);
		else{
			r.isEnd = true;
			r.value = t;
		}
		return r;
	}

	/** function to delete a key **/
	public T delete(String key){
		TSTNode n = delete(root, key.toCharArray(), 0);
		if(n != null)	return (T)n.value;
		else return null;
	}

	/** function to delete a key **/
	private TSTNode<T> delete(TSTNode r, char[] key, int ptr){
		if (r == null)				return null;
		if (key[ptr] < r.data)                return delete(r.left, key, ptr);
		else if (key[ptr] > r.data)           return delete(r.right, key, ptr);
		else{
		/** to delete a key just make isEnd false **/
		if (r.isEnd && ptr == key.length - 1)            {r.isEnd = false; return r;}
		else if (ptr + 1 < key.length)                   return delete(r.middle, key, ptr + 1);
		}
		return null;
	}
	public boolean contains(String key){
		return get(root, key.toCharArray(),0) != null;
	}
	
	public T get(String key)
	{
		TSTNode n = get(root, key.toCharArray(), 0);
		if(n != null)	return (T)n.value;
		else return null;
	}

	private TSTNode<T> get(TSTNode<T> r, char[] key, int ptr)
	{
		if (r == null)                return null;
		if (key[ptr] < r.data)       return get(r.left, key, ptr);
		else if (key[ptr] > r.data)  return get(r.right, key, ptr);
		else{
		if (r.isEnd && ptr == key.length - 1)           return r;
		else if (ptr == key.length - 1)                 return null;
		else			                         return get(r.middle, key, ptr + 1);
		}        
	}

	public String toString()
	{
		al = new ArrayList<String>();
		traverse(root, "");
		return "\nTernary Search Tree : "+ al;
	}

	private void traverse(TSTNode r, String str)
	{
		if (r != null)
		{
		traverse(r.left, str);
		str = str + r.data;
		if (r.isEnd)                   al.add(str);
		traverse(r.middle, str);
		str = str.substring(0, str.length() - 1);
		traverse(r.right, str);
		}
	}
}