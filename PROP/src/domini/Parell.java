package domini;

public class Parell<Tipus1,Tipus2> {
	private Tipus1 primer;
	private Tipus2 segon;
	
	// Creadores
	public Parell() {}
	
	public Parell (Tipus1 p, Tipus2 s) {
		primer = p;
		segon = s;
	}
	
	// Consultores
	public Tipus1 ObtenirPrimer() {
		return primer;
	}
	
	public Tipus2 ObtenirSegon() {
		return segon;
	}
	
	//Modificadores
	public void CanviarPrimer(Tipus1 p) {
		primer = p;
	}
	
	public void CanviarSegon(Tipus2 s) {
		segon = s;
	}
	
	public void CopiarParell(Parell<Tipus1,Tipus2> parell) {
		primer = parell.ObtenirPrimer();
		segon = parell.ObtenirSegon();
	}	
}
