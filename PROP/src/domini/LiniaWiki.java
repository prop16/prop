package domini;

public class LiniaWiki {
	private String NodeOrigen;
	private String TipusNodeOrigen;
	private String Contingut;
	private String NodeDesti;
	private String TipusNodeDesti;
	
	// Constructores
	public LiniaWiki() {}
	
	public LiniaWiki(String s1, String s2, String s3, String s4, String s5){
		NodeOrigen = s1;
		TipusNodeOrigen = s2;
		Contingut = s3;
		NodeDesti = s4;
		TipusNodeDesti = s5;
	}
	
	// Consultores
	public String ObtenirLinia(){
		return (NodeOrigen + " " + TipusNodeOrigen + " " + Contingut + " " + NodeDesti + " " + TipusNodeDesti);
	}
	
	public String ObtenirNodeOrigen() {
		return NodeOrigen;
	}
	
	public String ObtenirTipusNodeOrigen() {
		return TipusNodeOrigen;
	}
	
	public String ObtenirContingut() {
		return Contingut;
	}
	
	public String ObtenirNodeDesti() {
		return NodeDesti;
	}
	
	public String ObtenirTipusNodeDesti() {
		return TipusNodeDesti;
	}
	
	// Modificadores
	public void CanviarNodeOrigen(String s) {
		NodeOrigen = s;
	}
	
	public void CanviarTipusNodeOrigen(String s) {
		TipusNodeOrigen = s;
	}
	
	public void CanviarContingut(String s) {
		Contingut = s;
	}
	
	public void CanviarNodeDesti(String s) {
		NodeDesti = s;
	}
	
	public void CanviarTipusNodeDesti(String s) {
		TipusNodeDesti = s;
	}
	
	public boolean equals(LiniaWiki l) {
		if (NodeOrigen.equals(l.NodeOrigen) && TipusNodeOrigen.equals(l.TipusNodeOrigen) && Contingut.equals(l.Contingut) && NodeDesti.equals(l.NodeDesti) && TipusNodeDesti.equals(l.TipusNodeDesti)) {
			return true;
		}
		else return false;
	}
}
