package domini;

public class GrafSenseArrelsException extends Exception {

	public GrafSenseArrelsException() {
		super();
	}
	
	public GrafSenseArrelsException(String missatge) {
		super(missatge);
	}
	
}	
