package domini;

public class ControladorTransformacioGraf{
	Graf<ElemWiki,ArestaPes,Double> output;
	AlgorismeDistancia dist;
	
	//Constructora
	public ControladorTransformacioGraf(){}
	
	//Mofificadores
	public void TriarAlgorismeDistancia(int numDistancia, Graf<ElemWiki,Relacio,String> g){
		Graf graf_wiki = g.clone();
		switch (numDistancia){
		case 1:
			dist = new Jaro_winkler(graf_wiki);
			break;
		case 2:
			dist = new damerau_levenshtein(graf_wiki);
			break;
		case 3:
			dist = new DistanciaLexica(graf_wiki);				
			break;

		}
	}
	
	public void modificar_graf_input(Graf<ElemWiki,Relacio,String> g){
		dist.modificar_graf_input(g.clone());
	}
	
	//Tipus jaro. Si no es defineixen utilitza els per defecte.
	public void parametres_jaro(double p, double llindar) throws DadesIncorrectesException{
		((Jaro_winkler) dist).modificar_p(p);
		
		((Jaro_winkler) dist).modificar_llindar(llindar);
		
	}
	
	public void AplicarAlgorismeDistancia() throws Exception {
		dist.convert();
		output = dist.obtenir_graf();
	}
	
	//Consultora
	public Graf<ElemWiki,ArestaPes,Double> obtenir_graf(){
		return output;
	}
	

}
