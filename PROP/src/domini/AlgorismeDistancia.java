package domini;

public abstract class AlgorismeDistancia {
	Graf<ElemWiki,ArestaPes,Double> graf_pes; 
	Graf<ElemWiki,Relacio,String> graf_wiki;
	
	//Constructora
	
	/*Crea un algorisme de distancia amb graf_wiki d'entrada g*/
	public AlgorismeDistancia(Graf<ElemWiki,Relacio,String> g){
		graf_wiki = g;
		graf_pes = new Graf<ElemWiki,ArestaPes,Double>(true);
	}
	
	//Retorna el graf resultat de aplicar l'algorimse
	public Graf<ElemWiki,ArestaPes,Double> obtenir_graf(){
		return graf_pes;
	}
	//Modifica el graf d'entrada
	public void modificar_graf_input(Graf<ElemWiki,Relacio,String> g){
		graf_wiki = g;
	}
	
	//Crea el graf de sortida graf_pes utilitzant el graf d'entrada graf_wiki
	public void convert() throws Exception {
		graf_pes = new Graf<ElemWiki,ArestaPes,Double>(true);
	}
	 

}
