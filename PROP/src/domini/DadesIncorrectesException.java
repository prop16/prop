package domini;

public class DadesIncorrectesException extends Exception {

	public DadesIncorrectesException() {
		super();
	}
	
	public DadesIncorrectesException(String missatge) {
		super(missatge);
	}
}
