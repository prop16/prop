package domini;

public abstract class Relacio extends Aresta<ElemWiki,String>{
	
	public Relacio() {
		super();
	}
	public Relacio(ElemWiki e1, ElemWiki e2, String s){
		super(e1,e2,s);
	}
}