package domini;

import java.util.List;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Comparator;


public class CliquePercolation extends AlgorismeCommunityDetection {
	
	public CliquePercolation(Entrada En, Sortida S) throws Exception {
		try {
                        CercarComunitats(En,S);
                }
                catch (Exception e) {
                        throw e;
                }
	}

	static class ComInteger implements Comparator<Integer> {
		public int compare(Integer i1, Integer i2) {
			if (i1 > i2) return 1;
			else if (i1 == i2) return 0;
			else return -1;
		}
	}
	
	private void CercarSubco_i(Graf G, List<List<Integer>> Subcomunitats, List<Integer> clq, PriorityQueue<Integer> nadj, List<Integer> pr, List<Integer> sr, int i, int k, double pestotal, double w, Sortida S) throws Exception {

	    if (i == k - 1 && !nadj.isEmpty()) {
	    
	    	List<Integer> Subcomunitat = new ArrayList<Integer>(clq);
	    	while(!nadj.isEmpty()) {
	    		double pes = pestotal;
	    		for (int q = 0; q < i; q++) {
	    			pes += (double)G.PesAresta(clq.get(q), sr.get(nadj.peek()));
	    		}
	    		double m = k*(k-1)/2;
	    		if (pes/m >= w) Subcomunitat.add(sr.get(nadj.poll()));
	    		else nadj.poll();
	    	}
	    	if (Subcomunitat.size() >= k) {
	    		
	    		// INFORMACIO ALGORISME
	    		String s = "Subcomunitat " + (Subcomunitats.size()+1) + " creada";
	    		S.AfegirPas(s);
	    		String aux = new String();
	    		for (int v = 0; v < Subcomunitat.size(); v++) {
	    			aux += Subcomunitat.get(v) + " ";
	    		}
	    		S.AfegirPas(aux);
	    		
	    		Subcomunitats.add(Subcomunitat);
	    	}
	    }  
	    else if (clq.size() - i <= nadj.size()) {
	    	while(!nadj.isEmpty()) { 
	    		clq.add(sr.get(nadj.poll()));										// Afegim a la possible Subcomunitat el node vei
	    		double pes = pestotal;
	    		for (int q = 0; q < i; q++) {
	    			pes += (double)G.PesAresta(clq.get(q), clq.get(i));
	    		}
	    		List<Integer> adj_j = new ArrayList<Integer>(); 
	    		
	    		try {
                                adj_j = G.VertexAdjacentsNum(clq.get(i));		// Creem una llista amb els nodes adjacents al node vei seleccionat
	    		}
	    		catch (Exception e) {
                                throw e;
	    		}
	    		Comparator<Integer> comparator = new ComInteger();
			PriorityQueue<Integer> nadjaux = new PriorityQueue<Integer>(adj_j.size(),comparator);
			for (int j = 0; j < adj_j.size(); j++) {
				if (pr.get(adj_j.get(j)) > pr.get(clq.get(i)) && nadj.contains(pr.get(adj_j.get(j)))) nadjaux.add(pr.get(adj_j.get(j)));
			}
	    		i++;
	    		try {
                                CercarSubco_i(G,Subcomunitats,clq,nadjaux,pr,sr,i,k,pes,w,S);		// Crida recursiva
	    		}
	    		catch (Exception e) {
                                throw e;
	    		}
	    		i--;
	    		clq.remove(i);
	    	}
	    }
	}
  
	private List<List<Integer>> CercarSubco(Graf G, int k, double w, List<Integer> pr, List<Integer> sr, Sortida S) throws Exception {
		
		List<List<Integer>> Subcomunitats = new ArrayList<List<Integer>>();
		for (int i = 0; i < sr.size(); i++) {
			
			// Creem una llista i hi guardem els vertexs adjacents al node que es troba en la posicio "i"
			List<Integer> adj_i = new ArrayList<Integer>();
			try {
                                adj_i = G.VertexAdjacentsNum(sr.get(i));
			}
			catch (Exception e) {
                                throw e;
			}
			// Creem una cua de prioritats que conte les posicions en l'ordenacio topologica dels nodes
			// adjacents al node "i" que es troben en posicions posteriors a ell. 
			Comparator<Integer> comparator = new ComInteger();
			PriorityQueue<Integer> nadj = new PriorityQueue<Integer>(adj_i.size(),comparator);
			for (int j = 0; j < adj_i.size(); j++) {
				if (pr.get(adj_i.get(j)) > i) nadj.add(pr.get(adj_i.get(j)));
			}
			
			if (nadj.size() >= k-1) {
				List<Integer> clq = new ArrayList<Integer>();
				clq.add(sr.get(i));
				double pestotal = 0;
				// Crida a la funcio recursiva que cerca Subcomunitats
				try {
                                        CercarSubco_i(G, Subcomunitats, clq, nadj, pr, sr, 1, k, pestotal, w,S);
                                }
                                catch (Exception e) {
                                        throw e;
                                }
			}
		}
		return Subcomunitats;
	}
	
	private boolean SonAdjacents(List<Integer> Subcomunitat1, List<Integer> Subcomunitat2, int N) {
		
		/**	Comprova la possible adjacencia de dues Subcomunitats. Els k-1 primers nodes de cada llista (Subcomunitat) son
		 * els principals, ja que conformen cadascun dels cliques amb la resta de nodes que segueixen. Per tant, el primer que
		 * es comprova es si en comparteixen k-2 d'iguals. Si es aixi, es busca a la resta de llista que n'hi hagi un altre
		 * d'igual, en el qual cas seran adjacents, ja que en total compartiran un (k-1)-clique. 
		 */
		int Ncom = 0;
		int i = 0;
		while (Ncom < N-1 && i < N) {
			int j = 0;
			while (Ncom < N-1 && j < N) {
				if (Subcomunitat1.get(i) == Subcomunitat2.get(j)) Ncom++;
				j++;
			}
			i++;
		}
		
		// Si no obtenim N-1 nodes principals iguals ja sabem que no son subcomunitats adjacents
		if (Ncom < N-1) return false;
		else {
			for (i = N; i < Subcomunitat1.size(); i++) {
				if (Subcomunitat2.contains(Subcomunitat1.get(i))) return true;
			}
			return false;
		}
		
	}
	
	private void CercaDeComunitat(List<List<Integer>> Comunitats,List<List<Integer>> Subcomunitats,int q, int k, List<Integer> NumSubCom, Sortida S) {
		
		/** A partir d'una Subcomunitat cerca Subcomunitats adjacents a ells que no tenen ja una Comunitat assignada. Cada cop que
		 * en troba una fa una crida recursiva per tal que la Subcomunitat que li es adjacent tambe busqui adjacencies. D'aquesta forma
		 * es troba tota una comunitat amb una sola crida inicial a la funcio.
		 */
		for (int i = 0; i < Subcomunitats.size(); i++) {
			if (NumSubCom.get(i) == -1 && SonAdjacents(Subcomunitats.get(q), Subcomunitats.get(i),k-1)) {
				String s = "Ajuntar Subcomunitat " + (i+1);
				S.AfegirPas(s);
				for (int j = 0; j < Subcomunitats.get(i).size(); j++) {
					NumSubCom.set(i, NumSubCom.get(q));
					if (!Comunitats.get(NumSubCom.get(q)).contains(Subcomunitats.get(i).get(j))) { 
						Comunitats.get(NumSubCom.get(q)).add(Subcomunitats.get(i).get(j));
					}
				}
				CercaDeComunitat(Comunitats,Subcomunitats,i,k,NumSubCom,S);
			}
		}
	}
  
	 // Consultores
	 protected void CercarComunitats(Entrada E, Sortida S) throws Exception {
	 
		 /** La primera part de l'algorisme consisteix en una cerca per trobar subcomunitats que continguin
		  * k o mes nodes. Cada subcomunitat ha de complir que tots els k-cliques que la conformen superen
		  * el pes llindar que es dona d'entrada. 
		  */
		 
		 // Llista de llistes d'enters per contenir les subcomunitats
		 List<List<Integer>> Subcomunitats = new ArrayList<List<Integer>>();
		 
		 String s1 = "- Comenca cerca de Subcomunitats";
		 S.AfegirPas(s1);
		 
		 // Metode que fa la cerca de Subcomunitats
		 try {
                        Subcomunitats = CercarSubco(E.Graf(),E.NombreNodesClq(), E.PesLlindar(),E.PrimeraRepresentacio(),E.SegonaRepresentacio(),S);
                }
                catch (Exception e) {
                        throw e;
                }
		 
		 
		 List<List<Integer>> Comunitats = new ArrayList<List<Integer>>();
		 List<Integer> NumSubCom = new ArrayList<Integer>(Subcomunitats.size());
		 for (int i = 0; i < Subcomunitats.size(); i++) NumSubCom.add(-1);
		 
		 /** La segona part de l'algorisme consisteix en ajuntar les Subcomunitats que siguin adjacents, 
		  * creant aixi les Comunitats definitives. Per ser adjacents nomes han de compartir un (k-1)-clique.
		  */
		 
		 String s2 = "- Comenca cerca de Comunitats ajuntant certes Subcomunitats";
		 S.AfegirPas(s2);
		 
		 int num = 0;
		 for (int i = 0; i < Subcomunitats.size(); i++) {
			 if (NumSubCom.get(i) == -1) {
				 
				 String s = "Cerca de Comunitat a partir de Subcomunitat " + (i+1);
				 S.AfegirPas(s);
				 
				 NumSubCom.set(i,num);
				 num++;
				 Comunitats.add(Subcomunitats.get(i));
				 // Metode que cerca Subcomunitats adjacents a la Subcomunitat "i" i les ajunta
				 CercaDeComunitat(Comunitats,Subcomunitats,i,E.NombreNodesClq(),NumSubCom,S);
			 }
		 }
		 
		 String s3 = "En total s'han trobat " + Comunitats.size() + " Comunitats";
		 S.AfegirPas(s3);
		 
		 // Crear comunitats
		 S.afegir_nodes(Comunitats);
	 }
}
