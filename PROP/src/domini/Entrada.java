package domini;

import java.util.List;

public class Entrada {
	Graf G;
	int kClique;
	int NombreComunitats;
	double pesllindar;
	List<Integer> PrimeraRepresentacio;
	List<Integer> SegonaRepresentacio;
	
	//Constructores
	public Entrada (Graf Gaux) {
		G = (Graf) Gaux.clone();
		kClique = -1;
	}
	
	// Constructora Clique Percolation
		public Entrada (Graf Gaux, int kclq, double pllind,List<Integer> pr, List<Integer> sr) throws DadesIncorrectesException {
                        
			if (kclq < 3 || kclq > 6) throw new DadesIncorrectesException("El nombre k dels cliques ha d'estar entre 3 i 6");
			if (pllind < 0 || pllind > 1) throw new DadesIncorrectesException("El nombre llindar ha d'estar entre 0 i 1");
			G = (Graf) Gaux.clone();
			kClique = kclq;
			pesllindar = pllind;
			PrimeraRepresentacio = pr;
			SegonaRepresentacio = sr;
		}
	
	// Constructora Newman-Girvan
	public Entrada (Graf Gaux, int ncom) throws DadesIncorrectesException {
		if (ncom < 1) throw new DadesIncorrectesException("El nombre de comunitats ha de ser major a 1");
		G = (Graf) Gaux.clone();
		NombreComunitats = ncom;
	}
	
	//Consultores
	public Graf Graf() {
		return G;
	}
	
	public Integer NombreNodesClq() {
		return kClique;
	}
	
	public Double PesLlindar() {
		return pesllindar;
	}
	
	public Integer NombreComunitats() {
		return NombreComunitats;
	}
	
	public List<Integer> PrimeraRepresentacio() {
		return PrimeraRepresentacio;
	}
	
	public List<Integer> SegonaRepresentacio() {
		return SegonaRepresentacio;
	}
	
	
}
