package domini;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


public class damerau_levenshtein extends AlgorismeDistancia{
	
	//Constructora
	public damerau_levenshtein(Graf<ElemWiki, Relacio, String> g) {
		super(g);

	}
	//Modificadores
	
	public void convert() throws Exception { 
		super.convert();
		int n = graf_wiki.ordre();
		boolean[] visitats = new boolean[n];
		Arrays.fill(visitats,false);
		for(int i = 0; i < n; ++i){

			LinkedList<Integer> q = new LinkedList<Integer>();
			if(!visitats[i]){
				q.add(i);
				visitats[i] = true;
			}
			while(!q.isEmpty()){
				int actual = q.remove();

				List<Integer> adjacents = new ArrayList<Integer>();
				adjacents = graf_wiki.VertexAdjacentsNum(actual);
				graf_pes.AfegirVertex(graf_wiki.vertex(actual));
				
				for(int j = 0; j < adjacents.size();++j){
					//Si no estan visitats s'afageixen
					int vei = adjacents.get(j);
					if(!visitats[vei]){
						double pes = 0;
						pes = calcularPes(graf_wiki.vertex(actual).NomNode(),graf_wiki.vertex(vei).NomNode());
						graf_pes.AfegirVertex(graf_wiki.vertex(vei));
						graf_pes.AfegirAresta(graf_wiki.vertex(actual), pes, graf_wiki.vertex(vei));
						visitats[vei] = true;
						q.add(vei);
					}
					//Altrament es mira si ja s'ha afegit l'aresta i en cas negatiu s'afegeix
					else{
						
						
						graf_pes.AfegirVertex(graf_wiki.vertex(vei));
						if(!graf_pes.ExisteixAresta(graf_wiki.vertex(actual), graf_wiki.vertex(vei))){

								double pes = calcularPes(graf_wiki.vertex(actual).NomNode(),graf_wiki.vertex(vei).NomNode());
								graf_pes.AfegirAresta(graf_wiki.vertex(actual), pes,graf_wiki.vertex(vei));
						}
						
						
					}
				}
			}
			
		}
		
		
		
	}
	
	//Metodes privats
	private double calcularPes(String A, String B){
		/*
			 versio no eficient i recursiva antiga (millor per entendre el funcionament)
		if(Math.min(i,j) == -1){
			return (double) Math.max(i,j);
		}

		else if(i > 0 && j > 0 && A.charAt(i) == B.charAt(j-1) && A.charAt(i-1) == B.charAt(j) ){
			double neqij;
			if( A.charAt(i)!= B.charAt(j)) neqij = 1.0;
			else neqij = 0.0;
			return Math.min( Math.min(dab(A,B,i-1,j) + 1.0 , dab(A,B,i,j-1) + 1.0), Math.min(dab(A,B,i-1,j-1) + neqij , dab(A,B,i-2,j-2)+1.0) );
		}

		else{
			double neqij;
			if( A.charAt(i)!= B.charAt(j)) neqij = 1.0;
			else neqij = 0.0;
			return Math.min( Math.min(dab(A,B,i-1,j) + 1.0, dab(A,B,i,j-1) +1.0) , dab(A,B,i-1,j-1) + neqij );
		}
		*/
        int[][] distance = new int[A.length() + 1][B.length() + 1];        
   	 
        for (int i = 0; i <= A.length(); i++)                                 
            distance[i][0] = i;                                                  
        for (int j = 1; j <= B.length(); j++)                                 
            distance[0][j] = j;                                                  
 
        for (int i = 1; i <= A.length(); i++)                                 
            for (int j = 1; j <= B.length(); j++)                             
                distance[i][j] = Math.min(Math.min(distance[i - 1][j] + 1, distance[i][j - 1] + 1),distance[i - 1][j - 1] + ((A.charAt(i - 1) == B.charAt(j - 1)) ? 0 : 1));

        return 1.0-((double)distance[A.length()][B.length()]/(double)Math.max(A.length(),B.length()));  // 1 - Distancia damerau / max(|A|,|B|) -> Similaritat paraules               
	}
}
